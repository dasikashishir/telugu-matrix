#!/usr/bin/bash
pandoc -s --toc --mathml  --css=sakura.css main.md 01.md 02.md -o index.html
pandoc -s --toc --mathjax  main.md 01.md 02.md --epub-cover-image=cover.png -o book.epub
